# CRM App

This is a simple Customer Management System for Laboratory/Scientific sales or consulting.

It uses:

- Django localflavors
- bootstrap-admin
- Crispy Forms (coming soon)
- django_admin_listfilter_dropdown

Right now, it just uses the native Django Admin + bootstrap as it's interface.
