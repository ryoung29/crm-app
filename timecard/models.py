from datetime import date

from django.db import models

from hotsheet.models import HotSheet


# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=25)
    client = models.ForeignKey(HotSheet, on_delete=models.DO_NOTHING,
                               limit_choices_to={'status': 3})
    rate = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return '{0} for {1}'.format(self.name, self.client.name)

    class Meta:
        ordering = ['name']


class Entry(models.Model):
    task = models.ForeignKey(Task, on_delete=models.DO_NOTHING)
    service_date = models.DateField(default=date.today)
    hours = models.DecimalField(max_digits=3, decimal_places=1)
    details = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return '{0} - {1} - {2} hours'.format(self.service_date,
                                              self.task.name,
                                              self.hours)

    @property
    def amount(self):
        return self.hours * self.task.rate

    class Meta:
        ordering = ['-service_date', 'task']
