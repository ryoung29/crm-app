# Generated by Django 2.0.3 on 2018-05-04 22:15

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('hotsheet', '0007_merge_20170809_1325'),
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('service_date', models.DateField(default=datetime.date.today)),
                ('hours', models.DecimalField(decimal_places=1, max_digits=3)),
            ],
            options={
                'ordering': ['-service_date', 'task'],
            },
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=25)),
                ('rate', models.DecimalField(decimal_places=2, max_digits=5)),
                ('client', models.ForeignKey(limit_choices_to={'status': 4}, on_delete=django.db.models.deletion.DO_NOTHING, to='hotsheet.HotSheet')),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.AddField(
            model_name='entry',
            name='task',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='timecard.Task'),
        ),
    ]
