import csv

from django.contrib import admin
from django_admin_listfilter_dropdown.filters import RelatedDropdownFilter
from django.http import HttpResponse

from hotsheet.admin import ExportCsvMixin
from .models import Task, Entry


# Register your models here.
# Site customizations
admin.site.site_title = "Lab Insights CRM"
admin.site.site_header = "Lab Insights CRM"
admin.site.index_title = "Lab Insights CRM"


class ExportTimecardCsvMixin:
    def timesheet_as_csv(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]
        field_names.append("rate")

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment: filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)

        for obj in queryset:
            row = [getattr(obj, field) for field in field_names if field != "rate"]
            row.append(obj.task.rate)
            writer.writerow(row)

        return response

    timesheet_as_csv.short_description = "Export Selected"


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin, ExportCsvMixin):
    search_fields = ['name']
    list_filter = (('client', RelatedDropdownFilter),)
    actions = ['export_as_csv']


@admin.register(Entry)
class EntryAdmin(admin.ModelAdmin, ExportTimecardCsvMixin):
    search_fields = ['task__name', 'task__client__name']
    list_filter = (('task', RelatedDropdownFilter),
                   ('service_date', admin.DateFieldListFilter))
    actions = ['timesheet_as_csv']
    autocomplete_fields = ('task', )
