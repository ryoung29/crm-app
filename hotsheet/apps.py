from django.apps import AppConfig


class HotsheetConfig(AppConfig):
    name = 'hotsheet'
