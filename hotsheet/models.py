import re
import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _
from localflavor.us import models as usmodels


class LabType(models.Model):
    lab_type = models.CharField(max_length=50, unique=True)

    def save(self, *args, **kwargs):
        super(LabType, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return "/lab_type/%s/" % self.id

    def __str__(self):
        return self.lab_type


class HotSheet(models.Model):
    STATUSES = [(0, "Cold"), (1, "Warm"), (2, "Hot"), (3, "Cooking")]
    LICENSE_TYPES = [("None", "None"), ("Accreditation", "Accreditation"),
                     ("Compliance", "Compliance"), ("Waiver", "Waiver"),
                     ("Registration", "Registration"), ("FDA", "FDA"),
                     ("Other", "Other")]

    name = models.CharField(max_length=100, verbose_name=_("Organization Name"))
    exp_date = models.DateField(blank=True, null=True, verbose_name=_("License Expiry Date"))
    clia_id = models.CharField(null=True, blank=True, max_length=10, verbose_name=_("CLIA ID"))
    phone = models.CharField(max_length=20, verbose_name=_("Phone"))
    email = models.EmailField(unique=False, null=True, blank=True, verbose_name=_("E-mail"))
    address = models.CharField(max_length=100, verbose_name=_("Address"), null=True, blank=True)
    city = models.CharField(max_length=50, verbose_name=_("City"), null=True, blank=True)
    state = usmodels.USStateField(default="CA", verbose_name=_("State"), null=True, blank=True)
    zip_code = usmodels.USZipCodeField(verbose_name=_("Zip code"), null=True, blank=True)
    recent_sanction = models.BooleanField(verbose_name=_("Recent Sanction?"), default=False)
    status = models.PositiveSmallIntegerField(choices=STATUSES, default=0)
    lab_type = models.ForeignKey(LabType, on_delete=models.CASCADE)
    comment = models.TextField(max_length=255, verbose_name=_("Comment"),
                               null=True, blank=True)
    license_type = models.CharField(max_length=25, choices=LICENSE_TYPES, default="None")
    comment = models.TextField(max_length=255, verbose_name=_("Comment"), null=True, blank=True)
    last_contacted = models.DateField(editable=False, blank=True, null=True,
                                      verbose_name=_("Date Last Contacted"))

    def clean_fields(self, exclude=None):
        if self.email:
            self.email = self.email.strip().lower()
        if self.address:
            self.address = self.address.strip().lower().title()

    def save(self, *args, **kwargs):
        self.clean_fields()
        super(HotSheet, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return "/hotsheet/%s/" % self.id

    def __str__(self):
        ret = "%s-%s: %s" % (
            self.name, self.get_status_display(), self.phone)
        if self.city:
            ret += " (%s)" % self.city
        return ret

    class Meta:
        ordering = ['name']


class Contact(models.Model):
    date = models.DateTimeField(verbose_name=_("Contact Date"), auto_now_add=True)
    organization = models.ForeignKey(HotSheet, on_delete=models.CASCADE)
    contacted = models.CharField(max_length=50, blank=True, null=True)
    call_back = models.BooleanField(default=False, verbose_name=_("Call Back?"))
    details = models.TextField(max_length=255, null=True, blank=True)
    heat_up = models.BooleanField(default=False, verbose_name=_("Heat up?"))
    cool_down = models.BooleanField(default=False, verbose_name=_("Cool down?"))

    class Meta:
        ordering = ['-date']

    def clean_fields(self, exclude=None):
        if self.contacted:
            self.contacted = self.contacted.strip().lower().title()

    def save(self, *args, **kwargs):
        self.clean_fields()
        super(Contact, self).save(*args, **kwargs)
        org = HotSheet.objects.get(pk=self.organization_id)
        org.last_contacted = self.date.date()
        if self.heat_up:
            if org.status < 4:
                org.status += 1
        if self.cool_down:
            if org.status > 0:
                org.status -= 1
        org.save()

    def get_absolute_url(self):
        return "/lab_type/%s/" % self.id

    def __str__(self):
        return "%s at %s on %s" % (self.contacted, self.organization, str(self.date))
