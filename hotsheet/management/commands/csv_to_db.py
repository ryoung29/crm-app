#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import sqlite3

from django.core.management.base import BaseCommand

from hotsheet.models import HotSheet, LabType


class Command(BaseCommand):
    help = 'Add items to hotsheet from csv file'

    def add_arguments(self, parser):
        parser.add_argument("filename", help='Name of csv file')

    def handle(self, *args, **options):
        # with sqlite3.connect("db.sqlite3") as conn:
            # cursor = conn.cursor()
        with open(options['filename'], "rt") as the_file:
            reader = csv.DictReader(the_file)
            for row in reader:
                lab_type = LabType.objects.get(lab_type=row['lab_type'])
                row['lab_type'] = lab_type
                if not row["last_contacted"]:
                    row["last_contacted"] = None
                item = HotSheet(**row)
                item.save()
                    # data = [
                        # row['license_type'], row['name'], row['address'], row['city'],
                        # row['state'], row['zip_code'], row['clia_id'], row['phone'],
                        # row['exp_date'], row['lab_type'], row['recent_sanction'],
                        # row['status'], row['comment']
                    # ]

                    # cursor.execute("""insert into hotsheet_hotsheet (
                        # license_type, name, address, city, state, zip_code,
                        # clia_id, phone, exp_date, lab_type_id, recent_sanction,
                        # status, comment)
                        # values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);""", data)
            # conn.commit()
