from django.test import TestCase
from hotsheet.models import HotSheet, Contact, LabType

# Create your tests here.

class HotsheetTestCase(TestCase):
    def setUp(self):
        LabType.objects.create(lab_type="Hospital")
        l1 = LabType.objects.get(pk=1)
        HotSheet.objects.create(name="Test Org", phone="777-333-9282",
                lab_type=l1, email="Test@Test.cOm",
                address="444 test ave.")

    def test_hotsheet_clean_fields(self):
        t1 = HotSheet.objects.get(pk=1)
        self.assertEqual(t1.address, "444 Test Ave.")
        self.assertEqual(t1.email, "test@test.com")


class ContactTestCase(TestCase):
    def setUp(self):
        LabType.objects.create(lab_type="Hospital")
        l1 = LabType.objects.get(pk=1)
        HotSheet.objects.create(name="Test Org", phone="777-333-9282",
                lab_type=l1, email="Test@Test.cOm",
                address="444 test ave.")
        t1 = HotSheet.objects.get(pk=1)
        Contact.objects.create(organization=t1, date='2017-01-01')

    def test_contact_last_contacted(self):
        t1 = HotSheet.objects.get(pk=1)
        c1 = Contact.objects.get(pk=1)
        self.assertEqual(c1.date, t1.last_contacted)

    def test_contact_heat_up(self):
        c1 = Contact.objects.get(pk=1)
        c1.heat_up = True
        c1.save()
        t1 = HotSheet.objects.get(pk=1)
        self.assertEqual(t1.status, 1)

    def test_contact_cool_down(self):
        c1 = Contact.objects.get(pk=1)
        c1.cool_down = True
        c1.save()
        t1 = HotSheet.objects.get(pk=1)
        self.assertEqual(t1.status, 0)
