# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-03 19:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hotsheet', '0004_auto_20170113_0132'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='hotsheet',
            options={'ordering': ['name']},
        ),
        migrations.AddField(
            model_name='contact',
            name='cool_down',
            field=models.BooleanField(default=False, verbose_name='Cool down?'),
        ),
        migrations.AddField(
            model_name='contact',
            name='heat_up',
            field=models.BooleanField(default=False, verbose_name='Heat up?'),
        ),
    ]
