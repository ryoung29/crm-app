import csv

from django.contrib import admin
from django_admin_listfilter_dropdown.filters import DropdownFilter, RelatedDropdownFilter
from django.http import HttpResponse

from .models import LabType, HotSheet, Contact

# Site customizations
admin.site.site_title = "Lab Insights CRM"
admin.site.site_header = "Lab Insights CRM"
admin.site.index_title = "Lab Insights CRM"

# Register your models here.
admin.site.register(LabType)


class ExportCsvMixin:
    def export_as_csv(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment: filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)

        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])

        return response

    export_as_csv.short_description = "Export Selected"


@admin.register(HotSheet)
class HotSheetAdmin(admin.ModelAdmin, ExportCsvMixin):
    search_fields = ['name', 'city', 'zip_code', ]
    list_filter = (
            ('state', DropdownFilter),
            ('status', DropdownFilter),
            ('license_type', DropdownFilter),
            ('lab_type', RelatedDropdownFilter))
    actions = ['export_as_csv']


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin, ExportCsvMixin):
    ordering = ('-date',)
    search_fields = ['organization__name', ]
    list_filter = (
            ('call_back', DropdownFilter), )
    actions = ['export_as_csv']
    autocomplete_fields = ('organization', )
