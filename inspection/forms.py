from django import forms
from django.contrib.admin.widgets import AdminRadioSelect

from .models import Answer


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ['accession', 'answer']
        widgets = {
            'answer': AdminRadioSelect,
        }
