from django.core.management.base import BaseCommand, CommandError

from hotsheet.models import HotSheet
from inspection.models import Answer, InspectionEvent, InspectionType, Question


class Command(BaseCommand):
    help = "Make a new Patient Tracer Audit event"

    def add_arguments(self, parser):
        parser.add_argument('client', type=int, help="Client ID Lab")

    def handle(self, *args, **options):
        try:
            contact = HotSheet.objects.get(pk=options['client'])
        except Exception as e:
            raise CommandError(str(e))

        it = InspectionType.objects.get(name="Tracer")
        questions = Question.objects.filter(inspection_type=it)
        event = InspectionEvent.objects.create(client=contact,
                                               inspection_type=it)
        answer_list = [Answer(event=event, question=question)
                       for question in questions]
        try:
            Answer.objects.bulk_create(answer_list)
        except Exception as e:
            raise CommandError(str(e))
