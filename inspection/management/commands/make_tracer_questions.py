from django.core.management.base import BaseCommand, CommandError

from inspection.models import InspectionType, Question


class Command(BaseCommand):
    help = "Make a new Patient Tracer Audit event"

    def handle(self, *args, **options):
        it, _ = InspectionType.objects.get_or_create(
            name="Tracer", defaults={"description": "Patient Tracer Audit"})

        QUESTIONS = [
            [1, "Pre-Analytical", "Specimen label legible first and last name-matches Req?"],
            [2, "Pre-Analytical", "Requisition complete/legible and available in EHR?"],
            [3, "Pre-Analytical", "Demographics entry complete/accurate?"],
            [4, "Pre-Analytical", "Tests ordered = Tests requested?"],
            [1, "Analytical", "Instrument printouts/worksheets = Reported Results?"],
            [2, "Analytical", "Abnormal results repeated (and documented) per policy?"],
            [3, "Analytical", "Preventative maintenance for each analyzer completed?"],
            [4, "Analytical", "Quality control for each test complete/acceptable?"],
            [5, "Analytical", "Do any results contradict each other? (ie, low BUN, high Crea?)"],
            [6, "Analytical", "Hematology only-Hgb/Hct ratio follow rule of three?"],
            [7, "Analytical", "Hematology only-Is MCHC > 36%"],
            [8, "Analytical", "Hgb and CHCM agree within 1 g/dL?"],
            [9, "Analytical", "Coag -Last calibration/lot #s/INR calculation-OK?"],
            [10, "Analytical", "CDC Reportables OK?"],
            [11, "Analytical", "Microbiology only-Micro ID QC Log OK?"],
            [1, "Post-Analytical", "All testing completed within stability?"],
            [2, "Post-Analytical", "TAT acceptable?"],
            [3, "Post-Analytical", "Critical results documented?"],
            [4, "Post-Analytical", "STAT results called/documented?"],
            [1, "Billing Review", "Tests ordered = tests billed?"],
            [2, "Billing Review", "Duplicate test codes billed?"],
            [3, "Billing Review", "Correct CPT codes billed?"],
            [4, "Billing Review", "Any panel tests ‘unbundled’?"],
            [5, "Billing Review", "If test order was ‘ambiguous’ was verified prior to order?"],
            [6, "Billing Review", "If ‘Shared’ testing performed, duplicate billed?"],
            [7, "Billing Review", "ICD-9 Documentation provided by client?"],
            [8, "Billing Review", "Right agency billed?  (ie, Medicare, Medicaid, Insurance)"],
            [9, "Billing Review", "Correct patient demographics/insurance info provided?"],
            [10, "Billing Review", "Any changes to billing info not on requisition are documented?"]]

        data = [{"inspection_type": it, "sorter": Q[0], "section": Q[1], "text": Q[2]}
                for Q in QUESTIONS]

        for d in data:
            Question.objects.create(**d)
