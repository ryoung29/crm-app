import os
import tempfile

from django.contrib import admin
from django_admin_listfilter_dropdown.filters import (DropdownFilter,
                                                      RelatedDropdownFilter)
from django.http import HttpResponse
from secretary import Renderer

from hotsheet.models import HotSheet
from hotsheet.admin import ExportCsvMixin
from . import models
from .forms import AnswerForm


# Site customizations
admin.site.site_title = "Lab Insights Inspection"
admin.site.site_header = "Lab Insights Inspection"
admin.site.index_title = "Lab Insights Inspection"


class EventReportMixin:
    def make_audit_report(self, request, queryset):
        """
        Make and audit report using a template
        """
        data = queryset.first()
        data = data.answer_set.select_related()
        engine = Renderer()
        root = os.path.dirname(__file__)
        document = root + '/templates/template.odt'
        result = engine.render(document, data=data)

        response = HttpResponse(content_type='application/vnd.oasis.opendocument.text; charset=ISO8859-1')
        response['Content-Disposition'] = 'inline; filename=audit_report.odt'

        with tempfile.NamedTemporaryFile() as output:
            output.write(result)
            output.flush()
            output = open(output.name, 'rb')
            response.write(output.read())

        return response

    make_audit_report.short_description = 'Make Audit Report'


# Register your models here.
class AnswerAdmin(admin.TabularInline):
    form = AnswerForm
    model = models.Answer
    extra = 0
    readonly_fields = ['accession']
    ordering = ['question', 'accession']


@admin.register(models.InspectionEvent)
class EventAdmin(admin.ModelAdmin, ExportCsvMixin, EventReportMixin):
    search_fields = ['client']
    list_display = ['client', 'inspection_date', 'status']
    list_filter = (
        ('inspection_date', admin.DateFieldListFilter),
        ('inspection_type', RelatedDropdownFilter),
        ('status', DropdownFilter))
    inlines = [AnswerAdmin, ]
    actions = ['export_as_csv', 'make_audit_report']

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        only show actual clients, not potentials
        """
        if db_field.name == 'client':
            kwargs['queryset'] = HotSheet.objects.filter(status=4)
        return super(EventAdmin, self).formfield_for_foreignkey(db_field,
                                                                request,
                                                                **kwargs)


@admin.register(models.Question)
class QuestionAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_filter = (
        ('inspection_type', RelatedDropdownFilter),
        ('section', DropdownFilter))
    actions = ['export_as_csv']


@admin.register(models.Answer)
class AnswerAdmin(admin.ModelAdmin, ExportCsvMixin):
    search_fields = ['event__client__name', 'question__text']
    list_display = ['event', 'inspection_type', 'section', 'sorter',
                    'question', 'answer']
    actions = ['export_as_csv']

    def inspection_type(self, obj):
        return obj.question.inspection_type.name

    def section(self, obj):
        return obj.question.section

    def sorter(self, obj):
        return obj.question.sorter


admin.site.register(models.InspectionType)
admin.site.register(models.Sample)
