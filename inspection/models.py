from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now

from hotsheet.models import HotSheet


# Create your models here.
class InspectionType(models.Model):
    name = models.CharField(max_length=10, verbose_name=_("Name"))
    description = models.CharField(max_length=255,
                                   verbose_name=_("Description"))

    class Meta:
        verbose_name = _("Inspection Type")
        verbose_name_plural = _("Inspection Types")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return "/type/%d/" % self.id


class Question(models.Model):
    inspection_type = models.ForeignKey(InspectionType, on_delete=models.CASCADE,
                                        verbose_name=_("Inspection Type"))
    section = models.CharField(max_length=25, verbose_name=_('Section'))
    text = models.CharField(max_length=50, verbose_name=_('Text'))
    sorter = models.PositiveSmallIntegerField()

    class Meta:
        unique_together = (("inspection_type", "section", "sorter"), )
        verbose_name = _("Question")
        verbose_name_plural = _("Questions")
        ordering = ["inspection_type", "section", "sorter"]

    def __str__(self):
        return self.text

    def get_absolute_url(self):
        return "/question/%d/" % self.id


class InspectionEvent(models.Model):
    client = models.ForeignKey(HotSheet, on_delete=models.CASCADE)
    inspection_type = models.ForeignKey(InspectionType, on_delete=models.CASCADE)
    inspection_date = models.DateField(default=now)
    status = models.CharField(max_length=10, default='Draft',
                              choices=(('Draft', 'Draft'),
                                       ('Active', 'Active'),
                                       ('Complete', 'Complete')))

    class Meta:
        ordering = ['-inspection_date', 'client']

    def __str__(self):
        return "{0}-{1}".format(self.client.name, self.inspection_date)

    def get_absolute_url(self):
        return "/event/%d/" % self.id


class Sample(models.Model):
    accession_number = models.CharField(max_length=20)
    date_collected = models.DateField(blank=True, null=True)
    date_reported = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.accession_number

    def get_absolute_url(self):
        return '/samples/%d/' % self.id


class Answer(models.Model):
    event = models.ForeignKey(InspectionEvent, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer = models.CharField(max_length=1,
                              choices=(('Y', 'Yes'), ('N', 'No'), ('X', 'N/A')),
                              help_text="Choose answer", default='X')
    accession = models.ForeignKey(Sample, null=True, blank=True, on_delete=models.CASCADE)

    def get_absolute_url(self):
        return "/event/%d/" % self.id

    def __str__(self):
        return "{0}: {1}".format(self.question, self.answer)
