from inspection.models import Question, Answer, InspectionEvent


def create_event(client, inspection_type, accessions=list()):

    try:
        questions = Question.objects.filter(inspection_type=inspection_type)
        event = InspectionEvent.objects.create(inspection_type=inspection_type,
                                               client=client)
        for accn in accessions:
            for q in questions:
                Answer.objects.create(accession=accn, event=event, question=q)
    except TypeError:
        print("Accession should be a list of accession objects")
