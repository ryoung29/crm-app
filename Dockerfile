# Use an official Python runtime as a parent image
FROM python:3.7-slim

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir --trusted-host pypi.python.org -r requirements.txt \
    && python manage.py makemigrations \
    && python manage.py migrate

# Make port 4001 available to the world outside this container
EXPOSE 4001

# Define environment variable
ENV NAME crm-app

# Run app.py when the container launches
CMD ["python3", "manage.py", "runserver", "0.0.0.0:4001"]
